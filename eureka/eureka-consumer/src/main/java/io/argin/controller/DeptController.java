package io.argin.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author argin
 * @since 2020-12-25
 */
@RestController
@RequestMapping("/plus/dept")
public class DeptController {
}
