package io.argin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.argin.entity.Emp;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author argin
 * @since 2020-12-25
 */
public interface IEmpService extends IService<Emp> {
}
