package io.argin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.argin.entity.Emp;
import io.argin.mapper.EmpMapper;
import io.argin.service.IEmpService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author argin
 * @since 2020-12-25
 */
@Service
public class EmpServiceImpl extends ServiceImpl<EmpMapper, Emp> implements IEmpService {

}
