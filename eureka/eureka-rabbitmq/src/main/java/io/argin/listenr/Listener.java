package io.argin.listenr;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class Listener {

    /**
     * 监听者接收消息三要素：
     * 1、queue
     * 2、exchange
     * 3、routing key
     */
    @RabbitListener(bindings =
    @QueueBinding(
            value = @Queue(value = "springboot_queue", durable = "true"),
            exchange = @Exchange(value = "springboot_exchange", type = ExchangeTypes.TOPIC),
            key = {"*.*"}
    ))
    public void listen(String msg) {
        System.out.println("接收到消息：" + msg);
    }
}