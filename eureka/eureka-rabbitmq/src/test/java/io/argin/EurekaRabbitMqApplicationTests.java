package io.argin;


import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EurekaRabbitMqApplicationTests {

    @Autowired
    private ConnectionFactory factory;

    @Test
    public void name() throws IOException, TimeoutException {
        Connection connection = factory.newConnection();
        System.out.println(connection);
    }
}
