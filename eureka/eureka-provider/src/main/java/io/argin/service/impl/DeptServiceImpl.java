package io.argin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.argin.entity.Dept;
import io.argin.mapper.DeptMapper;
import io.argin.service.IDeptService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author argin
 * @since 2020-12-25
 */
@Service
public class DeptServiceImpl extends ServiceImpl<DeptMapper, Dept> implements IDeptService {

}
