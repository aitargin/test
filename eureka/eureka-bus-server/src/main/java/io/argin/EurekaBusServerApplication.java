package io.argin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class EurekaBusServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaBusServerApplication.class, args);
    }

}
