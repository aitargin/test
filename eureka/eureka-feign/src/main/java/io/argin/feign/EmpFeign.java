package io.argin.feign;

import io.argin.entity.Dept;
import io.argin.entity.Emp;
import io.argin.fallback.EmpServiceFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@FeignClient(name="eureka-provider",fallback = EmpServiceFallback.class)
public interface EmpFeign {

    @GetMapping("/listD")
    List<Dept> listDepts();

    @PostMapping("/insert")
    void insertEmp(@RequestBody Emp emp);

    @PostMapping("/map/insert")
    void insertMap(@RequestBody Map<String, Object> map);

    @GetMapping("/delete/{id}")
    void deleteEmp(@PathVariable Integer id);

    @GetMapping("/get/{id}")
    Emp getEmp(@PathVariable Integer id);

    @GetMapping("/get")
    Emp getEmpById(@RequestParam Integer id);

    @PostMapping("/update")
    void updateEmp(@RequestBody Emp emp);

    @GetMapping("/list")
    List<Emp> listEmps();

    @GetMapping("/list/{name}")
    List<Emp> listEmpsByName(@PathVariable String name);
}
