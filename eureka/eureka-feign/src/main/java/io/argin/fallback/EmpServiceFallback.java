package io.argin.fallback;

import io.argin.entity.Dept;
import io.argin.entity.Emp;
import io.argin.feign.EmpFeign;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class EmpServiceFallback implements EmpFeign {
    @Override
    public List<Dept> listDepts() {
        return null;
    }

    @Override
    public void insertEmp(Emp emp) {

    }

    @Override
    public void insertMap(Map<String, Object> map) {

    }

    @Override
    public void deleteEmp(Integer id) {

    }

    @Override
    public Emp getEmp(Integer id) {
        Emp emp = new Emp();
        emp.setName("任勇蓁");
        emp.setSex("男");
        emp.setAge(22);
        emp.setDid(1);
        return emp;
    }

    @Override
    public Emp getEmpById(Integer id) {
        Emp emp = new Emp();
        emp.setId(0);
        emp.setName("任勇蓁");
        emp.setSex("男");
        emp.setAge(22);
        emp.setDid(1);
        return emp;
    }

    @Override
    public void updateEmp(Emp emp) {

    }

    @Override
    public List<Emp> listEmps() {
        return null;
    }

    @Override
    public List<Emp> listEmpsByName(String name) {
        return null;
    }
}
