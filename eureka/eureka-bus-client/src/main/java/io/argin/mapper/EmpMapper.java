package io.argin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.argin.entity.Emp;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author argin
 * @since 2020-12-25
 */
public interface EmpMapper extends BaseMapper<Emp> {
}
