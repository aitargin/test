package io.argin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

@MapperScan(basePackages = "io.argin.mapper")
@SpringCloudApplication
public class EurekaBusClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaBusClientApplication.class, args);
    }

}
