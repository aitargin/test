package io.argin.util;

import io.argin.entity.Emp;

import java.util.Map;

public class MapUtils {
    public static Emp parseEmp(Map map){
        Emp emp = new Emp();
        emp.setId((Integer)map.get("id"));
        emp.setName((String)map.get("name"));
        emp.setSex((String)map.get("sex"));
        emp.setAge((Integer)map.get("age"));
        emp.setDid((Integer)map.get("did"));
        return emp;
    }
}
