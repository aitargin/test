package io.argin.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataResourConf {

    @Bean
    @ConfigurationProperties(prefix = "md")
    public DruidDataSource getDruidDataSource() {
        return new DruidDataSource();
    }


}
