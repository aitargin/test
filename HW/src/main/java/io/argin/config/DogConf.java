package io.argin.config;

import io.argin.entity.Dog;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DogConf {

    @Bean(name = "a")
    @ConfigurationProperties(prefix = "dog.a")
    public Dog getA() {
        return new Dog();
    }

    @Bean(name = "b")
    @ConfigurationProperties(prefix = "dog.b")
    public Dog getB() {
        return new Dog();
    }

}
