package io.argin;

import io.argin.entity.Dog;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes={App.class})
public class DogTest {

    @Resource
    private Dog a;
    @Resource
    private Dog b;

    @Test
    public void name() {
        System.out.println(a);
        System.out.println(b);
    }
}
