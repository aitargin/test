package io.argin;

import com.alibaba.druid.pool.DruidDataSource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes={App.class})
public class DataResourceTest {

    @Autowired
    private DruidDataSource source;

    @Test
    public void name() {
        System.out.println(source);
        System.out.println(source.getUrl());
        System.out.println(source.getUsername());
        System.out.println(source.getPassword());
        System.out.println(source.getDriverClassName());
    }
}
