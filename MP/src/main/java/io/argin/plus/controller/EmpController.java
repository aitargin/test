package io.argin.plus.controller;


import io.argin.plus.entity.Emp;
import io.argin.plus.service.IEmpService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author argin
 * @since 2020-12-25
 */
@RestController
@RequestMapping("/plus/emp")
public class EmpController {
    @Resource
    private IEmpService service;

    @GetMapping("/get")
    public List<Emp> fun1(){
        return service.listAll();
    }

    @GetMapping("/put")
    public String fun2(){
        Emp emp = new Emp();
        try {
            emp.setName("高铭阳");
            emp.setDid(1);
            System.out.println(emp);
            service.save(emp);
            return "添加成功";
        } catch (Exception e) {
            e.printStackTrace();
            return "添加失败";
        }
    }
    @GetMapping("/upd")
    public String fun3(){
        Emp emp = new Emp();
        try {
            emp.setId(8);
            emp.setName("高铭阳2");
            emp.setAge(65);
            System.out.println(emp);
            service.updateById(emp);
            return "修改成功";
        } catch (Exception e) {
            e.printStackTrace();
            return "修改失败";
        }
    }
}
