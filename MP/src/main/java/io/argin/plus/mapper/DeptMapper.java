package io.argin.plus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.argin.plus.entity.Dept;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author argin
 * @since 2020-12-25
 */
public interface DeptMapper extends BaseMapper<Dept> {
}
