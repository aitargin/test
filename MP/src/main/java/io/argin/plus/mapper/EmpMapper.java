package io.argin.plus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.argin.plus.entity.Emp;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author argin
 * @since 2020-12-25
 */
public interface EmpMapper extends BaseMapper<Emp> {
    List<Emp> listAll();
}
