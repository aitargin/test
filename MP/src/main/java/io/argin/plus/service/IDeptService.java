package io.argin.plus.service;

import io.argin.plus.entity.Dept;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author argin
 * @since 2020-12-25
 */
public interface IDeptService extends IService<Dept> {

}
