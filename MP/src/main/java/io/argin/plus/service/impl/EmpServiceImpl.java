package io.argin.plus.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.argin.plus.entity.Emp;
import io.argin.plus.mapper.EmpMapper;
import io.argin.plus.service.IEmpService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author argin
 * @since 2020-12-25
 */
@Service
public class EmpServiceImpl extends ServiceImpl<EmpMapper, Emp> implements IEmpService {
    @Resource
    private EmpMapper mapper;

    @Override
    public List<Emp> listAll(){
        return mapper.listAll();
    }
}
