package io.argin.plus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.argin.plus.entity.Emp;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author argin
 * @since 2020-12-25
 */
public interface IEmpService extends IService<Emp> {
    List<Emp> listAll();
}
