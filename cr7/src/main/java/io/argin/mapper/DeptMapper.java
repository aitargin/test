package io.argin.mapper;

import io.argin.pojo.Dept;
import tk.mybatis.mapper.common.Mapper;

public interface DeptMapper extends Mapper<Dept> {
}
