package io.argin.mapper;

import io.argin.pojo.Emp;
import tk.mybatis.mapper.common.Mapper;

public interface EmpMapper extends Mapper<Emp> {
}
