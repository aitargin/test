package io.argin.service;

import io.argin.pojo.Dept;
import io.argin.pojo.Emp;

import java.util.List;

public interface QueryService {
    void insertEmp(Emp emp);
    void deleteEmp(Integer id);
    void updateEmp(Emp emp);
    Emp getEmp(Integer id);
    List<Emp> listEmps(String name);
    List<Dept> listDepts();
}
