package io.argin.pojo;

import lombok.Data;

import javax.persistence.Id;

@Data
public class Emp {
    @Id
    private Integer id;
    private String name;
    private String sex;
    private Integer age;
    private Integer did;
    private transient String dept;
}
