package io.argin.pojo;


import lombok.Data;

import javax.persistence.Id;

@Data
public class Dept {
  @Id
  private Integer id;
  private String name;
}
