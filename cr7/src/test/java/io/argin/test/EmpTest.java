package io.argin.test;

import io.argin.App;
import io.argin.mapper.EmpMapper;
import io.argin.service.QueryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

@SpringJUnitConfig
@SpringBootTest(classes = {App.class})
public class EmpTest {
    @Autowired
    EmpMapper mapper;
    @Autowired
    QueryService service;

    @Test
    public void listEmps() {
        mapper.select(null).forEach(System.out::println);
    }

    @Test
    public void listEmps2() {
        service.listEmps("张").forEach(System.out::println);
    }
}
